Prerrequisitos:
-    Tener instalado Java (https://www.java.com/es/download/).
-    Tener el puerto 6666 libre.

Pasos para la descarga:
-    Descargar las carpetas JAR Servidor, JAR Cliente.

Pasos para la ejecucion en un ordenador:
-    Entrar en la carpeta con nombre JAR Servidor y ejecutar el archivo con nombre Servidor.jar.
-    Salir de la carpeta JAR Servidor y entrar en la carpeta JAR Cliente.
-    Ejecutar el archivo con nombre  Cliente.jar dos veces (las ventanas se sobrepondran una encima de la otra). Por cada dos clientes, se ejecutará una partida.
-    Cuando se abran las pantallas del cliente, pinchar en jugar en cada una de ellas, y esperar a que se realicen las conexiones.
-    La partida se iniciara y se podra empezar a jugar.
-    Para finalizar la ejecución del servidor habrá que hacerlo desde el administrador de tareas.

Pasos para la ejecucion en dos ordenadores:
-    La red local debe estar conectada mediante un cable ethernet.
-    Entrar en la carpeta con nombre JAR Servidor y ejecutar el archivo con nombre Servidor.jar en uno de los ordenadores.
-    Salir de la carpeta JAR Servidor y entrar en la carpeta JAR Cliente.
-    Ejecutar el archivo con nombre  Cliente.jar en ambos ordenadores.
-    Cuando se abran las pantallas del cliente, pinchar en jugar en cada una de ellas, y esperar a que se realicen las conexiones.
-    La partida se iniciara y se podra empezar a jugar.
-    Para finalizar la ejecución del servidor habrá que hacerlo desde el administrador de tareas.