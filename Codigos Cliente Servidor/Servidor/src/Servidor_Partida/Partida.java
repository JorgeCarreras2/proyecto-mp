package Servidor_Partida;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Ataque;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Jugador;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Personaje;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Jugador.ClaseEscuchadoraAtaques;
import Servidor.PartidaConexiones;
import Servidor_Modelo.Modelo;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketException;
import java.sql.SQLException;

public class Partida extends Thread implements Serializable {

    private ArrayList<Jugador> arrayJugadores;
    private Coeficiente coeficiente;
    private Queue<Jugador> colaTurnoJugadores;
    private ClaseEscuchadoraAtaques listener;
    private ArrayList<Socket> socketsJugadores;
    private PartidaConexiones partidaConexiones;
    private final byte PERSONAJES_LUCHANDO_CURRENT_TIME = 1; // por jugador

    public Partida(ArrayList<Socket> socketsJugadores) throws SQLException {
        arrayJugadores = new ArrayList<>(socketsJugadores.size());
        this.socketsJugadores = socketsJugadores;
        Modelo modelo = new Modelo();

        for (int i = 0; i < 2; i++) {
            arrayJugadores.add(new Jugador(String.valueOf(socketsJugadores.get(i).getPort()), modelo.getPersonajes()));
        }

        this.coeficiente = new Coeficiente();
        this.colaTurnoJugadores = new LinkedList<Jugador>();
        this.partidaConexiones = new PartidaConexiones(socketsJugadores);
    }

    @Override
    public void run() {
        // tema conexiones
        try {
            elegirTurnoInicial(arrayJugadores); // Calcula a quien le toca atacar primero.
            partidaConexiones.enviarDatos(arrayJugadores);
            while (!finJuego()) { // mientras uno de los dos jhugadores no este derrotado la partida sigue.

                turno(partidaConexiones.recibirdatos());

                partidaConexiones.enviarDatos(arrayJugadores);

                // tenemos que matar el hilo y cerrar las conexiones tcp
            }
        } catch (SocketException e) {
            System.out.println("Un jugador ha abandonado la partida");
            System.out.println(e.getMessage());
            cerrarsockets();
            this.interrupt();
        } catch (IOException e) {
            System.out.println("Un jugador ha abandonado la partida");
            System.out.println(e.getMessage());
            cerrarsockets();
            this.interrupt();
        } catch (Exception e) {
            System.out.println("Ha habido un error inesperado en la partida, se cierra la partida");
            System.out.println(e.getMessage());
            cerrarsockets();
            this.interrupt();
        }

    }

    /**
     * Metodo que cierra todos los sockets en caso de errores. Si esta cerrado
     * pues ni nos molestamos
     */
    private void cerrarsockets() {
        for (Socket aux : socketsJugadores) {
            try {
                aux.close();
            } catch (IOException e) {

            }

        }

    }
    
    /**
     * Metodo que elige que jugador es que empezara atacando. El jugador que atacara primero sera el que tenga un primer personaje con mayor velocidad que el contrario.
     * @param jugadores 
     */

    private void elegirTurnoInicial(List<Jugador> jugadores) {

        TreeMap<Integer, Jugador> treeJugadores = new TreeMap<>();
        for (Jugador j : jugadores) {
            List<Personaje> aux = new ArrayList<>();
            aux.addAll(j.getPersonajes());

            if (PERSONAJES_LUCHANDO_CURRENT_TIME - 1 == 0) {// Obtengo los personajes que estan luchando en este primer
                // momento
                Personaje personaje = aux.get(0);
                aux = new ArrayList<Personaje>();
                aux.add(personaje);
            } else {
                aux = aux.subList(0, PERSONAJES_LUCHANDO_CURRENT_TIME - 1);// Obtengo los personajes que estan luchando en este primer momento
            }

            aux.sort((Personaje p1, Personaje p2) -> Integer.compare(p1.getVelocidad(), p2.getVelocidad())); // los ordeno y saco el mejor en velocidad
            Collections.reverse(aux);
            if (treeJugadores.containsKey(aux.get(0).getVelocidad())) {
                treeJugadores.put(treeJugadores.lastKey() + 1, j);
            } else {
                treeJugadores.put(aux.get(0).getVelocidad(), j); // asigno la velocidad de este al jugador
            }
        }

        for (Map.Entry<Integer, Jugador> entry : treeJugadores.entrySet()) { // ordeno a los jugadores por su velocidad.
            colaTurnoJugadores.add(entry.getValue());
        }

    }
    
    /**
     * Metodo que ejecuta un turno de la partida
     * @param hashMap 
     */

    private void turno(HashMap<String, Integer> hashMap) {

        IteracionesCombate(hashMap); // Primer atacante
        if (finJuego()) { // sino no le quedan personajes se acabo el juego
            return;
        }
        IteracionesCombate(hashMap);// Segundo atacante

        meHeDejadoAlgunMuertoPorAhi();

    }

    /**
     * Metodo que elimina de la lista de personajes de jugador los personajes que no tengan vida
     */
    
    private void meHeDejadoAlgunMuertoPorAhi() {

        for (Jugador j : arrayJugadores) {
            for (Personaje p : j.getPersonajes()) {
                if (!p.estaVivo()) {
                    j.getPersonajes().remove(p);
                    j.setPersonajeDerrotado(true);
                    break;
                }

            }
        }

    }

    /**
     * Metodo que Trabajo con los combates que se producen. Primero saco el
     * jugador que le toca jugar de la cola, despues se a�ade a la p�rte
     * inferior de la cola y se empieza con el combate
     *
     * @param hashMap
     */
    private void IteracionesCombate(HashMap<String, Integer> hashMap) {
        Jugador j = colaTurnoJugadores.poll();
        colaTurnoJugadores.add(j);
        combate(j, hashMap);
    }

    /**
     * meodo Combate que realiza las operaciones necesarias de ataque y resta de
     * vida para el jugador contrario. El Jugador atacante ataca al jugador que
     * tiene justo depues en el orden de prioridad.
     *
     * @param j Jugador que se encuentra atacando
     * @param hashMap
     */
    private void combate(Jugador j, HashMap<String, Integer> hashMap) {
        Ataque ataque = null;
        Jugador contrario = colaTurnoJugadores.peek();
        int tipoAtacante = 0;

        for (Personaje pAux : j.getPersonajes()) {
            if (pAux.estaVivo()) {
                tipoAtacante = pAux.getCodTipoGenerico();
                ataque = pAux.getAtaques().get(hashMap.get(j.getCodJugador()) - 1);
                j.setPersonajeDerrotado(false);
                break;
            } else {
                j.getPersonajes().remove(pAux);
                j.setPersonajeDerrotado(true);
                break;
            }
        }

        if (!j.getPersonajeDerrotado()) { //si el personaje esta derrotado no puede atacar, ademas peta porque java es inteligente
            for (Personaje pAux : contrario.getPersonajes()) {
                if (pAux.estaVivo()) {
                    pAux.recibeDamage(coeficiente.getDamageReal(tipoAtacante, pAux.getCodTipoGenerico(), ataque));
                    break;
                } else {
                    break;
                }
            }
        }

    }

    /**
     * Se comprueba si los jugadores les quedan personajes y se determina si se
     * ha acabado el juego o no.
     *
     * @return <code>true</code> si el juego ha terminado, else
     * <code>false</code>
     */
    private boolean finJuego() {
        for (Jugador jaux : arrayJugadores) {
            // setjugadorderrotado
            jugadorDerrotado(jaux);
            if (jaux.getJugadorDerrotado()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determina si al jugador pasado por parametro se ha quedado sin personajes
     * vivos.
     *
     * @param jaux {@link Jugador} a analizar.
     * @return <code>true</code> if el jugador ha sido derrotado, else
     * <code>false</code>
     */
    private boolean jugadorDerrotado(Jugador jaux) {

        for (Personaje paux : jaux.getPersonajes()) {
            if (paux.estaVivo()) {
                return false;
            }
        }
        return true;

    }

}
