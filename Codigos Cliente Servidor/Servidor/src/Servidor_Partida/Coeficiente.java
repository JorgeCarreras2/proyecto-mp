package Servidor_Partida;

import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Ataque;

/* La clase coefieciente es usada para dar el valor real del ataque que se
 * realiza sobre el contrario. La propiedad coef es la encargada de multipliacar
 * al danyo del ataque en las situaciones que explicaremos en el metodo
 * 
 * @see getDamageReal
 */
public class Coeficiente {

    private final double coef1 = 1.25;
        private final double coef2 = 0.75;

    /* 
         * Este metodo aplica un coeficiente sobre el damage del ataque del personaje segun si se ataca
         * a un personaje que es debil en la jerarquia de tipos respecto al nuestro, o si atacamos a 
         * un personaje que en la jerarquia es mas fuerte que nosotros. Esta jerarquía esta 
         * explicada debajo:
     * 
     * Ataque de tipo agua hace damage a personaje de tipo fuego. 
         * Ataque de tipo fuego hace damage a personaje de tipo planta. 
         * Ataque de tipo planta hace damage a personaje de tipo agua.
         * Ataque de tipo electrico hace damage a personaje de tipo agua.
     * 
         * Ejemplo 1: si un personaje con un ataque de tipo fuego ataca a
         * un personaje de tipo planta se le aplicará el coefieciente 1.
         * Ejemplo 2: por el contrario si un personaje con un tipo de ataque 
         * de tipo fuego ataca a un personaje de tipo agua se le aplicará al ataque el coeficioente 2. 
         * 
     * @param tipoAtacante tipo del personaje del jugador que esta atacando
     * @param tipoAtacado  tipo del personaje del contrario
     * @param ataque       ataque seleccionado por el jugador
     * @return devuelve el valor final del ataque
     */
public int getDamageReal(int tipoAtacante, int tipoAtacado, Ataque ataque) {
        double damage = ataque.getDamage();

        if (tipoAtacante == ataque.getCodTipoGenerico()) {
            damage = damage * this.coef1;
        }

        switch (tipoAtacante) {
        case 1:
            if (tipoAtacado == 3) {
                            damage = damage * this.coef1;
            }
                        if(tipoAtacado == 2){
                            damage = damage * this.coef2;
                        }
            break;
        case 2:
            if (tipoAtacado == 1) {
                            damage = damage * this.coef1;
            }
                        if(tipoAtacado == 3 || tipoAtacado ==4){
                            damage = damage * this.coef2;
                        }
            break;
        case 3:
            if (tipoAtacado == 2) {
                            damage = damage * this.coef1;
            }
                        if(tipoAtacado == 1){
                            damage = damage * this.coef2;
                        }
            break;
        case 4:
            if (tipoAtacado == 4) {
                damage = damage * this.coef1;
            }
            break;
        }
        return (int) damage;
    }
}