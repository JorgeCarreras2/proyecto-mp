package Servidor;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Jugador;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

	//Clase que tiene que hacer las conexiones etc
public class PartidaConexiones {

    private ArrayList<Socket> sockets;

    public PartidaConexiones(ArrayList<Socket> sockets) {
        this.sockets = sockets;
    }

    public HashMap<String, Integer> recibirdatos() throws IOException {
        HashMap<String, Integer> mapa = new HashMap<>();
        BufferedInputStream datos = null;

        for (Socket socketAux : sockets) {
            datos = new BufferedInputStream(socketAux.getInputStream());

            int ataque = 0;
            
                ataque = datos.read();
            

            mapa.put(String.valueOf(socketAux.getPort()), ataque);

        }

        return mapa;
    }

    /**
     * Metodo que envia los jugadores a los distintos jugadores.
     *
     * @param listaJugadores la lista de los jugadores que se encuentra en
     * partida con sus caracteriristicas Ver:{@link Jugador}
     * @throws IOException
     */
    public void enviarDatos(ArrayList<Jugador> listaJugadores) throws IOException {
        ObjectOutputStream objOut = null;
        for (Socket auxsocket : sockets) {
            OutputStream outputStream = auxsocket.getOutputStream();
            objOut = new ObjectOutputStream(outputStream);

            for (Jugador adj : listaJugadores) {
                objOut.writeObject(adj);
            }
        }
    }
}
