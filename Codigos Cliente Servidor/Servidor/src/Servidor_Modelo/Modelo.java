package Servidor_Modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Ataque;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Personaje;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.TipoGenerico;

import java.util.Collections;

public class Modelo {

    /**
     * Metodo que importa de base de datos los personajes con todo su contenido para un jugador.
     * @return lista de 3 personajes con sus ataques y tipos ya asignados.
     * @throws SQLException
     */
    public List<Personaje> getPersonajes() throws SQLException{
        ConexionConLaBaseDatos base = new ConexionConLaBaseDatos();
        base.conectar();
        List<Personaje> resultado = new ArrayList(3);
        
        List<Personaje> lista = base.getPersonajes();
        List<TipoGenerico> tipos = base.getTipos();
        List<Ataque> ataques = base.getAtaques();
        
        Collections.shuffle(lista);
        int num;
        for(int i=0;i<3;i++){
            resultado.add(lista.remove(0));
            num = resultado.get(i).getCodTipoGenerico();
            TipoGenerico aux = buscar(tipos, num);
            resultado.get(i).setTipoGenerico(aux);
        }
        
        Collections.shuffle(ataques);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                resultado.get(i).setAtaque(ataques.remove(0));
                num = resultado.get(i).getAtaque(j).getCodTipoGenerico();
                TipoGenerico aux = buscar(tipos, num);
                resultado.get(i).getAtaque(j).setTipoGenerico(aux);
            }
            resultado.get(i).setAtaque(base.getSpecial(resultado.get(i).getSpecial()));
            num = resultado.get(i).getAtaque(3).getCodTipoGenerico();
            TipoGenerico aux = buscar(tipos, num);
            resultado.get(i).getAtaque(3).setTipoGenerico(aux);
        }
        base.cerrar();
        
        return resultado;
    }
    
    /**
     * Metodo privado para la busqueda de un TipoGenerico en la lista de este tipo
     * @param tipos lista en la buscar el tipo
     * @param num codigo del TipoGenerico a buscar
     * @return el TipoGenerico que se buscaba
     */
    private TipoGenerico buscar(List<TipoGenerico> tipos, int num) {
        boolean encontrado= false;
        int i=-1;
        while (!encontrado && i<tipos.size()){
            i++;
            encontrado = tipos.get(i).getCodTipoGenerico() == num;
        }
        return (tipos.get(i));
    }

}
