package Servidor_Modelo;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Ataque;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Personaje;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.TipoGenerico;

import java.util.LinkedList;
import java.util.List;

public class ConexionConLaBaseDatos {

    private Connection conexion;
    private final String prueba = "baseDeDatos.db";

    /**
     * Metodo que conecta con la base de datos.
     *
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        this.conexion = null;

        this.conexion = DriverManager.getConnection("jdbc:sqlite:" + this.prueba);
        System.out.println("Conectado");
        /*
            if(conexion!=null){
                System.out.println("Conectado")
            }
         */

    }

    /**
     * Metodo que cierra la conexion con la base de datos.
     *
     * @throws SQLException
     */
    public void cerrar() throws SQLException {
        this.conexion.close();
    }

    /**
     * Metodo que realiza una consulta en la tabla character de la base de
     * datos.
     *
     * @return devuelve una lista con todos los personajes disponibles en la
     * tabla.
     * @throws SQLException
     */
    public List<Personaje> getPersonajes() throws SQLException {
        Personaje personaje = null;
        List<Personaje> lista = new LinkedList();

        PreparedStatement declaracion = this.conexion.prepareStatement("select * from character");
        ResultSet resultado = declaracion.executeQuery();
        while (resultado.next()) {
            int code = resultado.getInt("codCharacter");
            String name = resultado.getString("name");
            int hp = resultado.getInt("hp");
            int speed = resultado.getInt("speed");
            int type = resultado.getInt("type");
            int special = resultado.getInt("specialAttack");
            String img = resultado.getString("URL");
            personaje = new Personaje(code, name, hp, speed, type, special, img);
            lista.add(personaje);

        }

        return lista;
    }

    /**
     * Metodo que realiza una consulta en la tabla attacks de la base de datos.
     *
     * @return devuelve una lista con todos los ataques disponibles en la tabla.
     * @throws SQLException
     */
    public List<Ataque> getAtaques() throws SQLException {
        ResultSet resultado = null;
        Ataque ataque = null;
        List<Ataque> lista = new LinkedList();

        PreparedStatement declaracion = this.conexion.prepareStatement("select * from attacks");
        resultado = declaracion.executeQuery();
        while (resultado.next()) {
            int codAtaque = resultado.getInt("codAttack");
            String name = resultado.getString("name");
            int damage = resultado.getInt("damage");
            int type = resultado.getByte("type");
            ataque = new Ataque(codAtaque, name, damage, type);
            lista.add(ataque);
        }

        return lista;
    }

    /**
     * Metodo que realiza una consulta en la tabla types de la base de datos.
     *
     * @return devuelve una lista con todos los personajes disponibles en la
     * tabla.
     * @throws SQLException
     */
    public List<TipoGenerico> getTipos() throws SQLException {
        ResultSet resultado = null;
        TipoGenerico tipo = null;
        List<TipoGenerico> lista = new LinkedList();

        PreparedStatement declaracion = this.conexion.prepareStatement("select * from types");
        resultado = declaracion.executeQuery();
        while (resultado.next()) {
            int code = resultado.getInt(1);//"code");
            String name = resultado.getString(2);//"name");
            int color = resultado.getInt(3);//"color");
            tipo = new TipoGenerico(code, name, color);
            lista.add(tipo);
        }

        return lista;
    }

    /**
     * Metodo que realiza una consulta en la tabla SpecialAttack de la base de
     * datos.
     *
     * @param codAtaque recibe el codigo del ataque a buscar en la tabla
     * @return devuelve el ataque correspondiente al codigo consultado
     * @throws SQLException
     */
    public Ataque getSpecial(int codAtaque) throws SQLException {
        ResultSet resultado = null;
        Ataque ataque = null;

        PreparedStatement declaracion = this.conexion.prepareStatement("select * from SpecialAttack WHERE codAttack = ?");
        declaracion.setInt(1, codAtaque);
        resultado = declaracion.executeQuery();
        while (resultado.next()) {
            String name = resultado.getString("name");
            int damage = resultado.getInt("damage");
            int type = resultado.getInt("type");
            ataque = new Ataque(codAtaque, name, damage, type);
        }

        return ataque;
    }

}
