package Jugador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Ataque;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Jugador;
import Jugador_Vistas.*;

import Jugador_Vistas.PantallaFinal;
import java.util.concurrent.TimeUnit;

/**
 * Clase que se encarga de la comunicacion entre la vista y los datos recibidos
 * del servidor. Actua de clase intermedia entre la vista del jugador y el
 * servidor. El controlador tiene acceso a los JFrames que debe mostrar en cada
 * momento y a la clase conexion que es quien le va a proporcionar los datos del
 * servidor
 *
 * @author jorge
 *
 */
public class ControladorJugador {

    private ConexionJugador conexionJugador;
    private EsperandoConexiones esperandoConexionesJFrame;
    private String codJugador;
    private ArrayList<Jugador> jugadores;
    private JFrameMostrarCombate combate;
    private ClaseEscuchadoraAtaques escuchadora;
    private PantallaFinal pantallaFinal = null;

    /**
     * En el constructor voy inicializando las variables que voy necesitando, en
     * lo que esto ocurre se muestra el JFrame {@link EsperandoConexiones}
     *
     */
    public ControladorJugador() throws IOException {
    	

        this.conexionJugador = new ConexionJugador();
        	try {
                this.codJugador = this.conexionJugador.conectaJugador();
        	}catch(NullPointerException e) {
        		JOptionPane.showMessageDialog(null,"No se ha podido establecer conexion con nigun servidor, inicia uno",null, JOptionPane.WARNING_MESSAGE);
        	}
        this.escuchadora = new ClaseEscuchadoraAtaques();
        // this.combate = new JFrameMostrarCombate(this.escuchadora);
    	
		

    }

    /**
     * El metodo consiste en iniciar un pokemon en el frame, aportando a la
     * clase combate todas las entradas que necesita para mostrar un personaje.
     *
     * @param jugador de jugador se coje el primer personaje y se le pasan las
     * propiedades necesarias al metodo @see iniciarBicho de la clase @see
     * combate. El if separa el mostrar el personaje perteneciente al host, del
     * personaje contrincante contra el que se esta jugando.
     * @throws Exception 
     */
    private void iniciarBichoCompleto(Jugador jugador) throws Exception {
        if (this.codJugador.equals(jugador.getCodJugador())) {
            combate.iniciarBicho(jugador.getPersonajes().get(0).getVida(), 1, jugador.getPersonajes().get(0).getNombre());
            List<Ataque> ataques = jugador.getPersonajes().get(0).getAtaques();
            int i = 1;
            for (Ataque aux : ataques) {
                this.combate.iniciarAtaque(i, aux.getNombre(), aux.getDamage(), aux.getColor());
                i++;
            }
        } else {
            combate.iniciarBicho(jugador.getPersonajes().get(0).getVida(), 2, jugador.getPersonajes().get(0).getNombre());
        }
    }

    /**
     * Este metodo consiste en realizar un turno durante una partida
     *
     * @param jugador el turno se realiza en funcion al jugador que le toque
     * dicho turno
     * @return devuelbe un booleano que nos indica si en ese turno se ha
     * finalizado la partida
     * @throws Exception 
     */
    private boolean ProcesoParaUnJugador(Jugador jugador) throws Exception  {
        boolean finPartida = false;

        if (jugador.getPersonajeDerrotado()) {
            if (jugador.getJugadorDerrotado()) {
                combate.dispose();
                finPartida = true;
                this.pantallaFinal = new PantallaFinal(jugador.getCodJugador().equals(this.codJugador));
            } else {
                this.iniciarBichoCompleto(jugador);
            }
        } else {
            if (this.codJugador.equals(jugador.getCodJugador())) {
                this.combate.setVida(jugador.getPersonajes().get(0).getVida(), 1);
            } else {
                this.combate.setVida(jugador.getPersonajes().get(0).getVida(), 2);
            }
        }
        return finPartida;
    }

    /**
     * Metodo principal de la clase. Es el encargado de la ejecucion de la
     * partida en el host. Llama al resto de métodos de la clase. Cuenta @see
     * finPartida para saber cuando salir de los bucles de la partida.
     * Inicializa los personajes al principio de la partida, y asigna a los
     * jugadores su @see codJugador a traves de un objeto de tipo @see
     * ConexionJugador. 
     * 
     * Estos codigos de los jugadores corresponden al socket
     * que el servidor les asigna, para asi diferenciar los distintos host, y
     * por tanto distintos jugadores. Tambien se habilita el uso de botones,
     * para recibir una entrada del usuario, que se enviará al servidor mediante
     * la funcion de la clase @see conexionJugador mediante el metodo @see
     * enviarDatos() donde se enviaran el ataque seleccionado, y se recibira la
     * respuesta de la partida o servidor a traves de la funcion recibirDatos().
     * @throws Exception 
     */
    public void juego() {
        
        try {            
            this.esperandoConexionesJFrame = new EsperandoConexiones();

            boolean finPartida = false;

            jugadores = (ArrayList<Jugador>) conexionJugador.pedirPartidaAlServidor();

            //this.codJugador = Arrays.toString(this.conexionJugador.getSocket().getSession().getId());
            this.codJugador = (this.conexionJugador.getSocket().getLocalSocketAddress().toString());
            combate = new JFrameMostrarCombate(this.escuchadora);
            String[] parts = this.codJugador.split(":");
            this.codJugador = parts[1];
            esperandoConexionesJFrame.dispose();
            combate.setVisible(true);
            for (Jugador aux : this.jugadores) {
                this.iniciarBichoCompleto(aux);
            }

            while (!finPartida && !this.conexionJugador.getSocket().isClosed()) {

                this.combate.activarBotones();
                int ataqueaux;
                synchronized (this.escuchadora) {
                    this.escuchadora.wait(10000);
                    ataqueaux = this.escuchadora.getAtaque();
                    if (ataqueaux == 0) {
                        ataqueaux = (int) (Math.random() * 4) + 1;
                    }

                    conexionJugador.enviarDatos(ataqueaux);
                }
this.combate.desactivarBotones();

                this.jugadores = (ArrayList<Jugador>) conexionJugador.recibirDatos();

                for (Jugador aux : this.jugadores) {
                    Thread.sleep(1500);
                    if (!finPartida) {
                        finPartida = this.ProcesoParaUnJugador(aux);

                    }
                }

            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
            this.esperandoConexionesJFrame.dispose();          
            JOptionPane.showMessageDialog(
                    esperandoConexionesJFrame, "Un jugador abandono el barco",
                    "WARNING",
                    JOptionPane.WARNING_MESSAGE);
            this.combate.dispose();
            
        } catch (InterruptedException e) {
            this.esperandoConexionesJFrame.dispose();
            this.combate.dispose();
            JOptionPane.showMessageDialog(
                    esperandoConexionesJFrame, "Ha habido un error inesperado, consulte con su programador mas cercano.",
                    "WARNING.",
                    JOptionPane.WARNING_MESSAGE);

        } catch (Exception e) {
            this.esperandoConexionesJFrame.dispose();
            this.combate.dispose();
            JOptionPane.showMessageDialog(
                    esperandoConexionesJFrame, "Ha habido un error generico que puede ser muchas cosas, dejelo reposar por un tiempo.",
                    "WARNING.",
                    JOptionPane.WARNING_MESSAGE);

        }

    }
}
