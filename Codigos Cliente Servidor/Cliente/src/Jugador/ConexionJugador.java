package Jugador;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador.Jugador;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Clase que genera la conexion con el servidor y se encarga de la comonicacion
 * con el mismo
 *
 * @author jorge
 *
 */
public class ConexionJugador {

    private Socket socket = null;
    private final int PORT = 6666;
    private String host;

    /**
     * Contructor vacio
     *
     */
    public ConexionJugador(){
    	
    }
    
    /**
     * Conecta al jugador con el servidor.
     *
     * @throws IOException
     */
    public String conectaJugador() throws IOException {
        
        InetAddress address = InetAddress.getLocalHost();
       String[] primerafase = address.getHostAddress().split("//");
       String[] segundafrase = primerafase[0].split("\\.");
       String host1 = segundafrase[0]+"."+segundafrase[1]+"."+segundafrase[2]+".";
       Runtime r = Runtime.getRuntime();
       
           // Mandamos peticiones a las ip entre 1-254
           for (int i = 1; i < 255; i++) {
        	   
        	   host = host1+i;
               Process p = r.exec("CMD /C PING -n 1 -w 1 " + host );      //mandamos 1 peticion de 1 segundo
               InputStream is = p.getInputStream();
               InputStreamReader isr = new InputStreamReader(is);
               BufferedReader br = new BufferedReader(isr);

               boolean existe = false;
               String linea;
			while ((linea = br.readLine()) != null) {
                   if (linea.indexOf("TTL") > 0) {
                	   try {
                		   this.socket = new Socket(host,PORT);
                    	   return this.socket.getLocalSocketAddress().toString();
					} catch (Exception e) {
						// TODO: handle exception
					}
                	   
                   }
                	   
                   }
           }
		
      
       
        return this.socket.getLocalSocketAddress().toString();
    }

    /**
     * Metodo que le pide al servidor iniciar una partida. El jugador se conecta
     * al servidor y espera a que le llegue la confirmacion de que hay dos
     * jugadores y una partida lista para ser jugada.
     *
     * @return
     * @throws UnknownHostException
     * @throws java.lang.ClassNotFoundException
     * @throws IOException
     */
    public ArrayList<Jugador> pedirPartidaAlServidor() throws IOException, ClassNotFoundException {
        return (ArrayList<Jugador>) recibirDatos();
    }

    /**
     * Metodo que lee datos del servidor
     *
     * @return 
     * @throws IOException
     * @throws java.lang.ClassNotFoundException
     */
    public synchronized ArrayList<Jugador> recibirDatos() throws IOException, ClassNotFoundException  {
            ObjectInputStream serverToHost = new ObjectInputStream(this.socket.getInputStream());
            Jugador jugador = null;
            ArrayList<Jugador> jugadores = new ArrayList<>(2);
            do {
                jugador = (Jugador) serverToHost.readObject();
                jugadores.add(jugador);
            } while (jugadores.size() < 2);
            
            return jugadores;
    }

    private void sinconexion() {
		// TODO Auto-generated method stub
		
	}

	/**
     * Metodo que envia un entero a traves de un DataOutputStream.
     *
     * @param codAtaque codigo del ataque seleccionado por el host.
     * @throws IOException
     */
    public void enviarDatos(int codAtaque) throws IOException {
        //Aqui hay que programar el paso de parametros al servidor
        DataOutputStream hostToServer = new DataOutputStream(this.socket.getOutputStream());
        hostToServer.writeByte(codAtaque);
    }

    public Socket getSocket() {
        return socket;
    }

}
