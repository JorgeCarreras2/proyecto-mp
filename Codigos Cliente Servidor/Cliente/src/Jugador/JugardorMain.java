package Jugador;

import Jugador_Vistas.PantallaInicio;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class JugardorMain {

    /**
     * Clase que consiste en el main del jugador. Se crea un objeto de la clase
     * ControladorJugador y se llama al metodo juego que es el que ejecuta la
     * partida en el host.
     */
    public static void main(String[] args) {
        PantallaInicio pi = new PantallaInicio();

        while (pi.botonPulsado == false) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
        pi.dispose();

        ControladorJugador gammer;
        try {
            gammer = new ControladorJugador();
             gammer.juego();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"Arranca el servidor por favor");
        }
       

    }

}
