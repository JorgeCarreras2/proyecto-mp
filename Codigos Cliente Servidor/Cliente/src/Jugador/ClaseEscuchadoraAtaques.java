package Jugador;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author a.gavilanv.2017
 */
public class ClaseEscuchadoraAtaques implements ActionListener {
    byte ataque = 0;
    @Override
    public synchronized void actionPerformed(ActionEvent e) {
        JButton o = (JButton) e.getSource();
        this.ataque=Byte.parseByte(o.getName());
        notify();
    }
    
    /**
     *
     * @return devuelve el botón pulsado [1-4]
     */
    public byte getAtaque(){
    byte aux = ataque;
    ataque=0;
    return aux;
    
    }
}
