package Jugador_Vistas;

import Jugador.ClaseEscuchadoraAtaques;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;

public  class JFrameMostrarCombate extends JFrame {
 
    /**
     * Inicializa un botÃƒÂ³n con el nombre del ataque y su daÃƒÂ±o
     * @param numeroAtaque elige a quÃƒÂ© botÃƒÂ³n se asigna el ataque [1-4]
     * @param nombre nombre que tiene el ataque (se mostrarÃƒÂ¡)
     * @param damage daÃƒÂ±o que hace el ataque (se mostrarÃƒÂ¡)
     */
    public void iniciarAtaque(int numeroAtaque, String nombre, int damage,Color colorAtaque){
        switch (numeroAtaque){
            case 1: iniciarAtaqueAux(ataque1,nombre,damage,colorAtaque);
            break;
            case 2: iniciarAtaqueAux(ataque2,nombre,damage,colorAtaque);
            break;
            case 3: iniciarAtaqueAux(ataque3,nombre,damage,colorAtaque);
            break;
            case 4: iniciarAtaqueAux(ataque4,nombre,damage,colorAtaque);
            break;
        }
    }
    
    private void iniciarAtaqueAux(JButton boton, String nombre, int damage, Color colorAtaque){
        boton.setText(nombre + " " + damage);
        Border borde = BorderFactory.createLineBorder(colorAtaque);
        boton.setBorder(borde);
    }
            
    /**
     * Inicializa totalmente un bicho (a utilizar cuando se cambia de bicho)
     * @param vidaMax vida del bicho en cuestiÃƒÂ³n
     * @param jugador jugador dueÃƒÂ±o del bicho que se estÃƒÂ¡ modificando (1 0 2)
     * @param nombreBicho nombre del bicho en cuestiÃƒÂ³n
     */
    public void iniciarBicho(int vidaMax, int jugador,String nombreBicho) throws Exception{
       
        
        if (jugador==1){
        barraVida1.setMinimum(0);
        barraVida1.setMaximum(vidaMax);
        barraVida1.setValue(vidaMax);
        barraVida1.setName(String.valueOf(vidaMax));
        vidaLabel1.setText(vidaMax + "/" + vidaMax);
        nombreBicho1.setText(nombreBicho);
        ImageIcon imagenBicho = volteoHorizontal(nombreBicho); 
        imagenBicho1.setIcon(imagenBicho);
        imagenBicho1.setText("");  
        }
        else{ 
        ImageIcon imagenBicho = new ImageIcon(("imagenes/" + nombreBicho + ".png"));
        barraVida2.setMinimum(0);
        barraVida2.setMaximum(vidaMax);
        barraVida2.setValue(vidaMax);
        barraVida2.setName(String.valueOf(vidaMax));
        vidaLabel2.setText(vidaMax + "/" + vidaMax);
        nombreBicho2.setText(nombreBicho);
        imagenBicho2.setText("");   
        imagenBicho2.setIcon(imagenBicho);      
        } 
        
        //this.jPanel1.setVisible(true);
        }
    
    /**
     * Actualiza la vida al bicho del jugador
     * @param vidaActual valor al que serÃƒÂ¡ actualizada la vida
     * @param jugador numero de jugador al que se le estÃƒÂ¡ actualizando la vida (1 o 2)
     */
    public void setVida(int vidaActual,int jugador){
         if (jugador==1){
        barraVida1.setValue(vidaActual);
        vidaLabel1.setText(vidaActual + "/" + barraVida1.getName());
   
        
         }else{
        barraVida2.setValue(vidaActual);
        vidaLabel2.setText(vidaActual + "/" + barraVida2.getName());
         }
    }

    /**
     *
     * @param CEA El JFrame "Combate" necesita una ClaseEscuchadoraDeAtaques para saber quÃƒÂ© botÃƒÂ³n es el que se ha pulsado.
     */
    public JFrameMostrarCombate(ClaseEscuchadoraAtaques CEA) {
        initComponents();
        iniciarBotones(CEA);
    }
    
    private void iniciarBotones(ClaseEscuchadoraAtaques EA){
        ataque1.setName("1");
        ataque1.addActionListener(EA);
        ataque2.setName("2");
        ataque2.addActionListener(EA);
        ataque3.setName("3");
        ataque3.addActionListener(EA);
        ataque4.setName("4");
        ataque4.addActionListener(EA);
        
    }

    /**
     *Activa todos los botones
     */
    public void activarBotones(){
        ataque1.setEnabled(true);
        ataque2.setEnabled(true);
        ataque3.setEnabled(true);
        ataque4.setEnabled(true);
    }

    /**
     * Desactiva todos los botones
     */
    public void desactivarBotones(){
        ataque1.setEnabled(false);
        ataque2.setEnabled(false);
        ataque3.setEnabled(false);
        ataque4.setEnabled(false);
    }
    
    private ImageIcon volteoHorizontal(String nombreBicho) throws Exception{
    File eo = new File("imagenes/" + nombreBicho + ".png");
    InputStream isInput = new FileInputStream(eo);
    BufferedImage simg = ImageIO.read(isInput);
  
    int width = simg.getWidth();
    int height = simg.getHeight();
 
    BufferedImage mimg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    
    for(int y = 0; y < height; y++){
        for(int lx = 0, rx = width-1; lx < width; lx++, rx--){
      
        int p = simg.getRGB(lx, y);
        
        mimg.setRGB(rx, y, p);
      }
    }
    return new ImageIcon(mimg);
  }//main() ends here

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton3 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        barraVida1 = new javax.swing.JProgressBar();
        vidaLabel1 = new javax.swing.JLabel();
        barraVida2 = new javax.swing.JProgressBar();
        vidaLabel2 = new javax.swing.JLabel();
        nombreBicho1 = new javax.swing.JLabel();
        nombreBicho2 = new javax.swing.JLabel();
        ataque1 = new javax.swing.JButton();
        ataque2 = new javax.swing.JButton();
        ataque3 = new javax.swing.JButton();
        ataque4 = new javax.swing.JButton();
        imagenBicho1 = new javax.swing.JLabel();
        imagenBicho2 = new javax.swing.JLabel();

        jButton3.setText("jButton1");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(java.awt.Color.white);
        setIconImages(null);
        setMaximumSize(new java.awt.Dimension(800, 800));
        setMinimumSize(new java.awt.Dimension(800, 800));
        setResizable(false);

        barraVida1.setBackground(new java.awt.Color(204, 204, 204));
        barraVida1.setForeground(new java.awt.Color(0, 153, 0));
        barraVida1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        vidaLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        vidaLabel1.setText("vidaBicho1");

        barraVida2.setBackground(new java.awt.Color(204, 204, 204));
        barraVida2.setForeground(new java.awt.Color(0, 153, 0));
        barraVida2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        vidaLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        vidaLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        vidaLabel2.setText("vidaBicho2");
        vidaLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        nombreBicho1.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        nombreBicho1.setText("nombreBicho1");

        nombreBicho2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        nombreBicho2.setText("nombreBicho2");
        nombreBicho2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        ataque1.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        ataque1.setText("ataque1");
        ataque1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ataque1ActionPerformed(evt);
            }
        });

        ataque2.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        ataque2.setText("ataque2");
        ataque2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ataque2ActionPerformed(evt);
            }
        });

        ataque3.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        ataque3.setText("ataque3");
        ataque3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ataque3ActionPerformed(evt);
            }
        });

        ataque4.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        ataque4.setText("ataque4(ulti)");
        ataque4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ataque4ActionPerformed(evt);
            }
        });

        imagenBicho1.setText("imagenBicho1");
        imagenBicho1.setMaximumSize(new java.awt.Dimension(300, 300));

        imagenBicho2.setText("imagenBicho2");
        imagenBicho2.setMaximumSize(new java.awt.Dimension(300, 300));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ataque3, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barraVida1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombreBicho1)
                    .addComponent(vidaLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(imagenBicho1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ataque1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ataque4, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ataque2, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(barraVida2, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nombreBicho2)
                            .addComponent(vidaLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(imagenBicho2, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreBicho1)
                    .addComponent(nombreBicho2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(barraVida2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vidaLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(barraVida1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vidaLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(imagenBicho1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(imagenBicho2, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(93, 93, 93)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ataque1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ataque2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ataque3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ataque4, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 74, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ataque1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ataque1ActionPerformed
          
    }//GEN-LAST:event_ataque1ActionPerformed

    private void ataque2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ataque2ActionPerformed
  
    }//GEN-LAST:event_ataque2ActionPerformed

    private void ataque3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ataque3ActionPerformed

    }//GEN-LAST:event_ataque3ActionPerformed

    private void ataque4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ataque4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ataque4ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ataque1;
    private javax.swing.JButton ataque2;
    private javax.swing.JButton ataque3;
    private javax.swing.JButton ataque4;
    private javax.swing.JProgressBar barraVida1;
    private javax.swing.JProgressBar barraVida2;
    private javax.swing.JLabel imagenBicho1;
    private javax.swing.JLabel imagenBicho2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel nombreBicho1;
    private javax.swing.JLabel nombreBicho2;
    private javax.swing.JLabel vidaLabel1;
    private javax.swing.JLabel vidaLabel2;
    // End of variables declaration//GEN-END:variables
}