package Jugador_Vistas;

/**
 *
 * @author a.gavilanv.2017
 */
public class PantallaFinal extends javax.swing.JFrame {

    /**
     * Pantalla que muestra un mensaje de victoria o derrota al jugador
     * @param Victoria boolean que decide si lo que se mostrará es un mensaje de victoria (true) o de derrota (false)
     */
    public PantallaFinal(boolean Victoria) {
        initComponents(Victoria);
        this.setVisible(true);
    }



    @SuppressWarnings("unchecked")                         
    private void initComponents(boolean Victoria) {

        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        if (Victoria){
        jLabel1.setText("Has ganado");
        }else{
        jLabel1.setText("Has perdido");    
        }
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(164, 164, 164)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(149, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(132, 132, 132)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(147, Short.MAX_VALUE))
        );

        pack();
    }             


    // Declaración de variables                    
    private javax.swing.JLabel jLabel1;
                  
}
