package Jugador_Vistas;

/**
 * Pantalla que muestra un mensaje de error
 * @author Alejandro
 */
public class PantallaError extends javax.swing.JFrame {
    private javax.swing.JLabel mensajeError;
    
    /**
    * Pantalla que muestra un mensaje de error, con formato (Error: mensaje)
    * @param mensaje String que se mostrara por pantalla
    */
    public PantallaError(String mensaje){
        initComponents(mensaje);
        setVisible(true);
    }
    
    
    private void initComponents(String mensajePantalla) {

        mensajeError = new javax.swing.JLabel();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        mensajeError.setText("Error: " + mensajePantalla);
        mensajeError.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        mensajeError.setMaximumSize(new java.awt.Dimension(200, 200));
        add(mensajeError);
        setMaximumSize(new java.awt.Dimension(200, 2000));
        setMinimumSize(new java.awt.Dimension(200, 2000));
        setResizable(false);

    
}
}
