package Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador;

import java.awt.Color;
import java.io.Serializable;

public class TipoGenerico implements Serializable {
	private int codTipoGenerico;
	private String name;
	private Color color;

	public TipoGenerico(int codTipoGenerico, String name, int color) {
		this.codTipoGenerico = codTipoGenerico;
		this.name = name;
		this.color = Color.getColor("clr", color);
	}

	public TipoGenerico() {

	}

	public int getCodTipoGenerico() {
		return this.codTipoGenerico;
	}

	public Color getColor() {
		return this.color;
	}
}
