package Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador;

import java.awt.Color;
import java.io.Serializable;
import javax.lang.model.element.VariableElement;

/**
 * Clase tipo Ataque que contiene todo lo referido a los ataques de los
 * personajes.
 */
public class Ataque implements Serializable {
	/**
	 * Codigo que permite identificar el ataque.
	 */
	private int codAtque;
	/**
	 * Nombre que permite mostrar el ataque en el JFrame
	 */
	private String nombre;
	/**
	 * Dano del ataque
	 */
	private int damage;
	/**
	 * Codigo que identifica el tipo del personaje
	 */
	private int codTipoGenerico;
	/**
	 * Variable {@link TipoGenerico} que guarda las especificaciones del tipo
	 */
	private TipoGenerico tipoGenerico;

	/*
	 * DLCs private int coolDown;
	 */

	/**
	 * Contructor de la clase {@link Ataque} que inicializa todos los parametros de
	 * un ataque
	 * 
	 * @param codAtaque       hace referencia al ataque, identificador del mismo.
	 * @param nombre          hace referencia al nombre del ataque
	 * @param damage          hace referencia al dano que realizara de base el
	 *                        ataque
	 * @param codTipoGenerico codigo que identifica el tipo del ataque.
	 */
	public Ataque(int codAtaque, String nombre, int damage, int codTipoGenerico) {
		this.codAtque = codAtaque;
		this.nombre = nombre;
		this.damage = damage;
		this.codTipoGenerico = codTipoGenerico;
	}

	// GETTER Y SETTERS
	// no se comenta
	public int getDamage() {
		return this.damage;
	}

	/*
	 * public TipoGenerico getTipoGenerico(){ return this.tipoGenerico; }
	 */

	public int getCodTipoGenerico() {
		return this.codTipoGenerico;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setTipoGenerico(TipoGenerico tipoGenerico) {
		this.tipoGenerico = tipoGenerico;
	}

	public Color getColor() {
		return this.tipoGenerico.getColor();
	}
}
