package Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador;

import java.util.List;

import java.io.Serializable;

/**
 * Clase que guarda a un jugador con sus personajes correspondientes
 * 
 * @category Modelo
 */
public class Jugador implements Serializable {
	/**
	 * Lista de {@link Personaje}
	 */
	private List<Personaje> personajes;
	/**
	 * Codigo que identifica al jugador
	 */
	private String codJugador;
	/**
	 * Variable que identifica si el jugador ha sido derrotado
	 */
	private boolean jugadorDerrotado;
	/**
	 * variable que identifica si al jugador le han derrotado algun personaje
	 */
	private boolean personajeDerrotado; // esto deberia estar en personaje pero mira es lo que hay

	/**
	 * Contructor de {@link Jugador} que inicializa los personajes y el codigo del
	 * jugador
	 * 
	 * @param codJugador codigo de identificador del jugador
	 * @param personajes lista de personajes del jugador
	 */
	public Jugador(String codJugador, List<Personaje> personajes) {
		this.codJugador = codJugador;
		this.personajes = personajes;
	}

	/*
	 * GETTER AND SETTERS NO COMMENTS
	 */
	public void setPersonajes(List<Personaje> personajes) {
		this.personajes = personajes;
	}

	public String getCodJugador() {
		return this.codJugador;
	}

	public List<Personaje> getPersonajes() {
		return personajes;
	}

	public void setPersonajeDerrotado(boolean e) {
		personajeDerrotado = e;
	}

	public void setJugadorDerrotado(boolean e) {
		jugadorDerrotado = e;
	}

	public boolean getJugadorDerrotado() {
		return personajes.size() == 0;
	}

	public boolean getPersonajeDerrotado() {
		return this.personajeDerrotado;
	}

    public static class ClaseEscuchadoraAtaques {

        public ClaseEscuchadoraAtaques() {
        }
    }
}
