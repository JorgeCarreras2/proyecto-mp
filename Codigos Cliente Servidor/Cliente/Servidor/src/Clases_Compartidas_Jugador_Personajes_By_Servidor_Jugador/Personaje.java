package Clases_Compartidas_Jugador_Personajes_By_Servidor_Jugador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * Clase que almacena los detalles de un personaje. 
 * @category Modelo
 *
 */
public class Personaje implements Serializable {
	/**
	 * Codigo que identifica el personaje
	 */
	private int codPersonaje;
	/**
	 * Nombre del {@link Personaje}
	 */
	private String nombre;
	/**
	 * Salud del {@link Personaje}
	 */
	private int vida;
	/**
	 * Velocidad del {@link Personaje}
	 */
	private int velocidad;
	/**
	 * codigo que identifica el tipo del personaje
	 * @see TipoGenerico
	 */
	private int codTipoGenerico;
	/**
	 * Detalles del tipo del personaje
	 * @see TipoGenerico
	 */
	private TipoGenerico tipoGenerico;
	/**
	 * Numero de atque especial
	 */
	private int especial; 
	/**
	 * Lista de ataques que tiene el {@link Personaje}
	 * @see Ataque
	 */
	private List<Ataque> ataques;
	/**
	 * ruta de la imegen que le pertenece al {@link Personaje}
	 */
	private String imagen;
	/**
	 * Contructor que inicializa un {@link Personaje} con sus parametros.
	 * 
	 * @param codPersonaje
	 * @param nombre
	 * @param vida
	 * @param velocidad
	 * @param codTipoGenerico
	 * @param especial
	 * @param imagen
	 */
	public Personaje(int codPersonaje, String nombre, int vida, int velocidad, int codTipoGenerico, int especial,
			String imagen) {
		this.codPersonaje = codPersonaje;
		this.nombre = nombre;
		this.vida = vida;
		this.velocidad = velocidad;
		this.codTipoGenerico = codTipoGenerico;
		this.especial = especial;
		ataques = new ArrayList<Ataque>(4);
		this.imagen = imagen;
	}

	public void recibeDamage(int damage) {
		if (this.vida > damage) {
			this.vida = this.vida - damage;
		} else {
			this.vida = 0;
		}

	}

	public void setAtaque(Ataque ataque) {
		ataques.add(ataque);
	}

	public int getCodPersonaje() {
		return this.codPersonaje;
	}

	public int getCodTipoGenerico() {
		return this.codTipoGenerico;
	}

	/*
	 * public TipoGenerico getTipoGenerico(){ return this.tipoGenerico; }
	 */

	public void setTipoGenerico(TipoGenerico tipoGenerico) {
		this.tipoGenerico = tipoGenerico;
	}

	public int getVida() {
		return vida;
	}

	public String getNombre() {
		return nombre;
	}

	public List<Ataque> getAtaques() {
		return this.ataques;
	}

	public Ataque getAtaque(int i) {
		return this.ataques.get(i);
	}

	public int getVelocidad() {
		return this.velocidad;
	}

	public String getImagen() {
		return this.imagen;
	}

	public boolean estaVivo() {
		return this.vida > 0;
	}



	public int getSpecial() {
		return this.especial;
	}
}