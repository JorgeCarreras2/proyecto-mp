package Servidor;

import javax.swing.JOptionPane;



public class ArrancaServidor {

	/**
	 * Clase que arranca el Servidor. 
	 * Simplemente se inicializa el arranque del Servidor. 
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		Servidor sv = new Servidor();
		try {
                        JOptionPane.showMessageDialog(null,"Servidor arrancado");
			sv.servicionIni();
        
		} catch (Exception e) {
			try {
                                JOptionPane.showMessageDialog(null,e.getMessage());
				sv.cerrarServidor();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

}
