package Servidor;

import java.util.ArrayList;
import Servidor_Partida.Partida;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Clase Servidor, inicia una partida cada dos conexiones que acpeta. La Clase
 * utiliza una conexion segura TCP.
 *
 * @author jorge
 *
 */
public class Servidor {

    private ServerSocket server = null;
    private Socket socket = null;
    private String HOST = "localhost";
    private int PORT = 6666;
    private ArrayList<Socket> jugadores = null;
    private PartidaConexiones partidaConexiones=null;

    public Servidor() {
        // TODO Auto-generated constructor stub
    }

    /**
     *  Metodo que se encarga de realizar la conexion con los dos Host
     * @throws Exception 
     */
    
    public void servicionIni() throws Exception {
        this.server = new ServerSocket(PORT);
        
        
        socket = new Socket(HOST, PORT);
        socket = server.accept();
        
    
        while (true) {
            jugadores = new ArrayList<>();
            //se reciben 2 jugadores
            for (int i = 0; i < 2; i++) {
                socket = server.accept();
                jugadores.add(socket);
            }
            //una vez que se tienen los dos jugadores se crea la partida. 
            System.out.println("Creando partida");
            Partida p = new Partida(jugadores);
            p.start();
        }
    }

    /**
     * Metodo que cierra el servidor
     * @throws Exception 
     */
    
    public void cerrarServidor() throws Exception {
        socket.close();
    }
}
